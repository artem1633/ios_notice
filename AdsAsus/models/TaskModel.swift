//
//  TaskModel.swift
//  AdsAsus
//
//  Created by Abdullox on 8/26/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import SwiftyJSON

class TaskModel {
    var id: String = ""
    var name: String = ""
    var address: String = ""
    var expiredDate: String = ""
    var description: String = ""
    var status: Int = 1
    var number : String = ""
    var hasPriority = false
    var user: String = ""
    var images = [UIImage]()
    
    
    static func parse(data: JSON) -> TaskModel{
        let model = TaskModel()
        
        if let id = data["id"].string{
            model.id = id
        }
        if let date = data["execute_datetime"].string{
            model.expiredDate = date
        }
        if let descr = data["description"].string{
            model.description = descr
        }
        if let status = data["status"]["id"].int{
            model.status = status
        }
        if let address = data["house"]["address"].string{
            model.address = address
        }
        if let user = data["executor"]["fio"].string{
            model.user = user
        }
        if let priority = data["priority"].string, priority == "1"{
            model.hasPriority = true
        }
        //for _ in 0..<data[]
        
        return model
    }
    
    static func parseArray(data: [JSON]) -> [TaskModel]{
        
        var items : [TaskModel] = []
        
        for item in data{
            items.append(TaskModel.parse(data: item))
        }
        
        return items
    }
    
    static func parseStatus(data: JSON) -> [Int: [TaskModel]]{
        guard let list = data.array else{
            return [:]
        }
        
        var items : [Int : [TaskModel]] = [:]
        
        for i in 1...6{
            items[i] = []
        }
        
        for element in list{
            let model = TaskModel.parse(data: element)
            
            items[model.status]?.append(model)
        }
                
        return items
    }
}
