//
//  OdpuModel.swift
//  AdsAsus
//
//  Created by Abdullox on 8/26/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import SwiftyJSON

class OdpuModel {
    var id: String = ""
    var apartment_id: String = ""
    var note: String = ""
    var number: String = ""
    var type: String = ""
    var date_verify: String = ""
    var address: String = ""
    var value: String = ""
    
    
    
    
    static func parse(data: JSON) -> OdpuModel{
        let model = OdpuModel()
        print("data: \(data)")
        
        if let id = data["id"].string{
            model.id = id
        }
        if let apartment_id = data["apartment_id"].string{
            model.apartment_id = apartment_id
        }
        if let number = data["number"].string{
            model.number = number
        }
        if let type = data["type"].string{
            model.type = type
        }
        if let note = data["note"].string{
            model.note = note
        }
        if let date_verify = data["date_verify"].string{
            model.date_verify = date_verify
        }
        if let house_address = data["house_address"].string{
            model.address = house_address
        }
        if let value = data["value"].string{
            model.value = value
        }
        
        print("model: \(model.value)")
        
        return model
    }
    
    static func parseArray(data: [JSON]) -> [OdpuModel]{
        var items : [OdpuModel] = []
        
        for item in data{
            items.append(OdpuModel.parse(data: item))
        }
        
        return items
    }
}
