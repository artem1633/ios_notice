//
//  StatusModel.swift
//  AdsAsus
//
//  Created by Abdullox on 8/26/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation

class StatusModel{
    
    enum Status : Int{
        case new = 1
        case inprocess
        case ended
        case cancel
        case archieve
        case expired
    }
    
    static let statuses = [
        Status.new.rawValue : "Новое",
        Status.inprocess.rawValue : "В работе",
        Status.ended.rawValue : "Решено",
        Status.cancel.rawValue : "Отменено",
        Status.archieve.rawValue : "Архивировано",
        Status.expired.rawValue : "Просрочено"
    ]
    
    
}
