//
//  FlameModel.swift
//  AdsAsus
//
//  Created by Abdullox on 8/26/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import SwiftyJSON

class FlameModel {
    var id: String = ""
    var name: String = ""
    var startDate: String = ""
    var endDate: String = ""
    var endFact: String = ""
    var resource: String = ""
    var status: String = ""
    var home: String = ""
    var address: String = ""
    
    
    static func parse(data: JSON) -> FlameModel{
        let model = FlameModel()
        
        if let id = data["id"].string{
            model.id = id
        }
        if let date = data["start_datetime"].string{
            model.startDate = date
        }
        if let date = data["plan_end_datetime"].string{
            model.endDate = date
        }
        if let fact = data["fact_end_datetime"].string{
            model.endFact = fact
        }
        if let status = data["status"].string{
            model.status = status
        }
        if let res = data["resource"].string{
            model.resource = res
        }
        if let house = data["house_id"].string{
            model.home = house
        }
        
        if let house = data["house"]["address"].string{
            model.address = house
        }
        
        return model
    }
    
    static func parseArray(data: [JSON]) -> [FlameModel]{
        var items : [FlameModel] = []
        
        for item in data{
            items.append(FlameModel.parse(data: item))
        }
        
        return items
    }
}
