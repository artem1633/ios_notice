//
//  FlameDetailVC.swift
//  AdsAsus
//
//  Created by Abdullox on 9/29/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class FlameDetailVC: UIViewController {
    
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endDateField: UITextField!
    @IBOutlet weak var endFactField: UITextField!
    @IBOutlet weak var resourceLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var homeLabel: UILabel!
    
    
    let datePicker = UIDatePicker()
    
    var model : FlameModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        startLabel.text = model.startDate
        endDateField.text = model.endDate
        endFactField.text = model.endFact
        homeLabel.text = model.home
        
        switch model.resource {
        case "0":
            resourceLabel.text = "Газ"
        case "1":
            resourceLabel.text = "Холодная вода"
        case "2":
            resourceLabel.text = "Электричество"
        case "3":
            resourceLabel.text = "Отопление"
        case "5":
            resourceLabel.text = "Горячая вода"
            
        default:
            break;
        }
        
        switch model.status {
        case "0":
            statusLabel.text = "Новый"
        case "1":
            statusLabel.text = "В работе"
        case "2":
            resourceLabel.text = "Ликвидировано"
        
        default:
            break;
        }

    
        setupDatePicker()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func changeEndDate(_ sender: Any) {
        endDateField.becomeFirstResponder()
    }
    
    
    @IBAction func changeFactDate(_ sender: Any) {
        endFactField.becomeFirstResponder()
    }
    
    
    func setupDatePicker() {

        // Specifies intput type
        datePicker.datePickerMode = .dateAndTime

        // Creates the toolbar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()

        // Adds the buttons
        var doneButton = UIBarButtonItem(title: "Выбрать", style: .plain, target: self, action: #selector(doneClick))
        var spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        var cancelButton = UIBarButtonItem(title: "Отмена", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        // Adds the toolbar to the view
        endDateField.inputView = datePicker
        endDateField.inputAccessoryView = toolBar
        
        endFactField.inputView = datePicker
        endFactField.inputAccessoryView = toolBar
    }

    @objc func doneClick(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"

        
        if endDateField.isEditing {
            endDateField.text = dateFormatter.string(from: datePicker.date)
            model.endDate = dateFormatter.string(from: datePicker.date)
        }else{
            endFactField.text = dateFormatter.string(from: datePicker.date)
            model.endFact = dateFormatter.string(from: datePicker.date)
        }
        
        updateCrashDisable()
        cancelClick(self)
    }

    @objc func cancelClick(_ sender: Any) {
        endDateField.resignFirstResponder()
        endFactField.resignFirstResponder()
    }
    

}



extension FlameDetailVC{
    func updateCrashDisable(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_FLAME_UPDATE, method: .post, parameters: [
            "token" : UserDefaults.userToken,
            "id": model.id,
            "plan_end_datetime": model.endDate,
            "fact_end_datetime": model.endFact
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.alertMessage(message: "Успешно")
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
}
