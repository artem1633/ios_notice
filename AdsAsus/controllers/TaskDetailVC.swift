//
//  TaskDetailVC.swift
//  AdsAsus
//
//  Created by Abdullox on 8/26/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import AVFoundation

class TaskDetailVC: UIViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate{
    
    enum HomeDetailStatus {
        case info
        case subtasks
        case comments
        case files
    }
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var userLabel: UILabel!
    
    var model : TaskModel!
    
    var status : HomeDetailStatus = .info
    
    var isAudioRecordingGranted: Bool!

    var tasks : [TaskModel] = []
    var comments : [(user: String, comment: String)] = []
    var files : [(image: String?, user: String, voice: String?)] = []
    
    var statuses : [(id: String, value: String)] = []
    var selectedStatus : Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.title = model.address

        addressLabel.text = model.address
        dateLabel.text = model.expiredDate
        descriptionLabel.text = model.description
        statusTextField.text = StatusModel.statuses[model.status]
        userLabel.text = model.user

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.backgroundColor = #colorLiteral(red: 0.9499999881, green: 0.9499999881, blue: 0.9499999881, alpha: 1)
        tableView.separatorStyle = .none
        
        tableView.register(UINib(nibName: "TaskTableCellTableViewCell", bundle: nil), forCellReuseIdentifier: "taskTableCellTableViewCell")
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "id")
        
        tableView.register(UINib(nibName: "TaskImageCell", bundle: nil), forCellReuseIdentifier: "TaskImageCell")
        
        tableView.register(UINib(nibName: "TaskSoundCell", bundle: nil), forCellReuseIdentifier: "TaskSoundCell")
        
        statusTextField.delegate = self
        configurePickerView()
        // Do any additional setup after loading the view.
        check_record_permission()
    }
    
    func check_record_permission()
    {
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSessionRecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSessionRecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (allowed) in
                    if allowed {
                        self.isAudioRecordingGranted = true
                    } else {
                        self.isAudioRecordingGranted = false
                    }
            })
            break
        default:
            break
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    func configurePickerView(){
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
         
        let toolbar = UIToolbar()
                
        toolbar.sizeToFit()
                
        let doneButton = UIBarButtonItem(title: "Выбрать", style: .plain, target: self, action: #selector(pickerViewDoneClicked))
                
        toolbar.setItems([doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        
        
        statusTextField.inputView = pickerView
        statusTextField.inputAccessoryView = toolbar
        
    }
    
    @objc func pickerViewDoneClicked(sender: Any){
        view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func segmentItemChanged(_ sender: Any) {
        if segmentControl.selectedSegmentIndex == 0{
            infoView.isHidden = false
            tableView.isHidden = true
        }else{
            infoView.isHidden = true
            tableView.isHidden = false
        }
        
        switch segmentControl.selectedSegmentIndex {
        case 0:
            self.navigationItem.rightBarButtonItem = nil
            status = .info
        case 1:
            self.navigationItem.rightBarButtonItem = nil
            status = .subtasks
        case 2:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addComment))
            status = .comments
        case 3:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addFile))
            status = .files
        default:
            break
        }
        
        tableView.reloadData()
    }
    
    @objc func addFile() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "ФОТО ИЗ КАМЕРЫ", style: .default) { _ in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        actionSheet.addAction(photoAction)
        let photoLibraryAction = UIAlertAction(title: "ФОТО ИЗ БИБЛИОТЕКИ", style: .default) { _ in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)

        }
        actionSheet.addAction(photoLibraryAction)
        let voiceAction = UIAlertAction(title: "ЗАПИСЬ ЗВУКА", style: .default) { _ in
            self.presentVoiceActionSheet()
        }
        actionSheet.addAction(voiceAction)
        let closeAction = UIAlertAction(title: "ЗАКРЫТЬ", style: .cancel) { _ in
            print("close")
        }
        actionSheet.addAction(closeAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK:- AUDIO RECORDER
    
    var audioRecorder: AVAudioRecorder!
    var audioPlayer : AVAudioPlayer!
    
    func getDocumentsDirectory() -> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    func getFileUrl() -> URL
    {
        let filename = "myRecording.m4a"
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
    return filePath
    }
    
    func setup_recorder()
    {
        if isAudioRecordingGranted
        {
            let session = AVAudioSession.sharedInstance()
            do
            {
                try session.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)
                try session.setActive(true)
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
                ]
                audioRecorder = try AVAudioRecorder(url: getFileUrl(), settings: settings)
                audioRecorder.delegate = self
                audioRecorder.isMeteringEnabled = true
                audioRecorder.prepareToRecord()
            }
            catch let error {
            }
        }
        else
        {
        }
    }
    
    func finishAudioRecording(success: Bool)
    {
        if success
        {
            audioRecorder.stop()
            audioRecorder = nil
        }
    }
    
    private func presentVoiceActionSheet(){
        let actionSheetVoice = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let startAction = UIAlertAction(title: "НАЧАТЬ ЗАПИСЬ", style: .default) { [self] _ in
            print("start")
            setup_recorder()
            audioRecorder.record()
            self.presentRecording()
        }
        actionSheetVoice.addAction(startAction)
        let closeAction = UIAlertAction(title: "ЗАКРЫТЬ", style: .cancel) { _ in
            print("close")
        }
        actionSheetVoice.addAction(closeAction)
        self.present(actionSheetVoice, animated: true, completion: nil)
    }
    
    private func presentRecording(){
        let actionSheetVoice2 = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let stopAction = UIAlertAction(title: "ОСТАНОВИТЬ ЗАПИСЬ", style: .default) { [self] _ in
            print("stop")
            finishAudioRecording(success: true)
            self.presentResult()
        }
        actionSheetVoice2.addAction(stopAction)
        let closeAction = UIAlertAction(title: "ЗАКРЫТЬ", style: .cancel) { _ in
            print("close")
        }
        actionSheetVoice2.addAction(closeAction)
        self.present(actionSheetVoice2, animated: true, completion: nil)
    }
    
    func prepare_play()
    {
        do
        {
            audioPlayer = try AVAudioPlayer(contentsOf: getFileUrl())
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
        }
        catch{
            print("Error")
        }
    }

    
    
    private func presentResult(){
        let actionSheetVoice3 = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let listen = UIAlertAction(title: "ПРОСЛУШАТЬ", style: .default) { [self] _ in
            if FileManager.default.fileExists(atPath: getFileUrl().path)
            {
                prepare_play()
                audioPlayer.play()
            }
            
            self.sendResult()
        }
        actionSheetVoice3.addAction(listen)
        let send = UIAlertAction(title: "ОТПРАВИТЬ", style: .default) { _ in
            print("send")
        }
        actionSheetVoice3.addAction(send)
        let closeAction = UIAlertAction(title: "ЗАКРЫТЬ", style: .cancel) { _ in
            print("close")
        }
        actionSheetVoice3.addAction(closeAction)
        self.present(actionSheetVoice3, animated: true, completion: nil)
    }
    
    private func sendResult(){
        let actionSheetVoice4 = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let send = UIAlertAction(title: "ОТПРАВИТЬ", style: .default) { _ in
            print("send")
            self.postVoice(audioPath: self.getFileUrl())
        }
        actionSheetVoice4.addAction(send)
        let closeAction = UIAlertAction(title: "ЗАКРЫТЬ", style: .cancel) { _ in
            print("close")
        }
        actionSheetVoice4.addAction(closeAction)
        self.present(actionSheetVoice4, animated: true, completion: nil)
    }
    //MARK: - ADD FROM LIBRARY
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let selectedImage = info[.editedImage] as? UIImage {
            postImage(image: selectedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    @objc func addComment() {
        let ac = UIAlertController(title: "Новый комментарий", message: nil, preferredStyle: .alert)
        ac.addTextField()

        let submitAction = UIAlertAction(title: "Отправить", style: .default) { [unowned ac] _ in
            let comment = ac.textFields![0].text
            self.postComment(comment: comment)
        }
        
        let closeAction = UIAlertAction(title: "Закрыть", style: .cancel)

        ac.addAction(closeAction)
        ac.addAction(submitAction)

        present(ac, animated: true)
    }
    
    @IBAction func submitClicked(_ sender: Any) {
        if selectedStatus != -1 {
            updateTask()
        }
        
        
    }
}


extension TaskDetailVC : UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return statuses.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return statuses[row].value
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedStatus = row
        statusTextField.text = statuses[row].value
    }
    
    
}


extension TaskDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch status {
        case .info:
            return 0
        case .subtasks:
            return tasks.count
        case .comments:
            return comments.count
        case .files:
            return files.count
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch status {
        case .subtasks:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "taskTableCellTableViewCell", for: indexPath) as? TaskTableCellTableViewCell{
                cell.model = tasks[indexPath.row]
                return cell
            }
        case .comments:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "commentTableCell", for: indexPath) as? CommentTableCell{
                cell.model = comments[indexPath.row]
                return cell
            }
            
            
        case .files:
            if files[indexPath.row].voice == nil {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "TaskImageCell", for: indexPath) as? TaskImageCell{
                    cell.taskLabel.text = files[indexPath.row].user
                    cell.taskImage.load(url: Constants.BASE_URL + files[indexPath.row].image!)
                    return cell
                }
            }
            if files[indexPath.row].image == nil {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "TaskSoundCell", for: indexPath) as? TaskSoundCell{
                    cell.taskLabel.text = files[indexPath.row].user
                    cell.sound = Constants.BASE_URL + files[indexPath.row].voice!
                    return cell
                }
            }
        default:
            return UITableViewCell()
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch status {
        case .subtasks:
            tableView.deselectRow(at: indexPath, animated: true)
            let st = UIStoryboard(name: "Main", bundle: nil)
            let vc = st.instantiateViewController(identifier: "taskDetailVC") as! TaskDetailVC
            vc.model = tasks[indexPath.row]
            
            navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
        
        
    }
    
}


extension TaskDetailVC{
    
    func getData(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_TASKS_DETAIL, method: .get, parameters: [
            "token" : UserDefaults.userToken,
            "id": model.id
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func postVoice(audioPath: URL){
        SVProgressHUD.show()
        
        
        Alamofire.upload(multipartFormData: {
                    (multiFormData) in
                        multiFormData.append(audioPath, withName: "file",fileName: "myRecording.m4a", mimeType: "audio/mpeg")

            multiFormData.append(UserDefaults.userToken.data(using: String.Encoding.utf8)!, withName: "token")
            multiFormData.append(self.model.id.data(using: String.Encoding.utf8)!, withName: "taskId")
            
                    },
        to: Constants.URL_POST_VOICE, method: .post) { (result) in
            print(result)
            SVProgressHUD.dismiss()
        }
        getData()
        tableView.reloadData()
    }
    
    func postImage(image: UIImage){
        SVProgressHUD.show()
        
        let imgData = image.jpegData(compressionQuality: 0.8)!
        
        
        Alamofire.upload(multipartFormData: {
                    (multiFormData) in
                        multiFormData.append(imgData, withName: "file",fileName: "image.jpg", mimeType: "image/jpg")

            multiFormData.append(UserDefaults.userToken.data(using: String.Encoding.utf8)!, withName: "token")
            multiFormData.append(self.model.id.data(using: String.Encoding.utf8)!, withName: "taskId")
            
                    },
        to: Constants.URL_POST_PHOTO, method: .post) { [self] (result) in
            getData()
            tableView.reloadData()
            print(result)
            SVProgressHUD.dismiss()
        }
        
        
        
        
        
        /*
        let req = Alamofire.request(Constants.URL_POST_PHOTO, method: .post, parameters: [
            "token" : UserDefaults.userToken,
            "taskId": model.id,
            "file": (imgData, withName: "file",fileName: "image.jpg", mimeType: "image/jpg")
        ])
        
        makeRequest(request: req) { [self] (response) in
            switch response{
            case .success(let data):
                if let errors = data["errors"].array{
                    errors.forEach { (error) in
                        self.alertMessage(message: error.stringValue)
                    }
                    
                }
                if let success = data["success"].bool, success{
                    self.alertMessage(message: "Готово")
                }
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
            getData()
            tableView.reloadData()
        }
 */
    }
    
    func postComment(comment: String?){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_POST_COMMENT, method: .post, parameters: [
            "token" : UserDefaults.userToken,
            "taskId": model.id,
            "text": comment ?? ""
        ])
        
        makeRequest(request: req) { [self] (response) in
            switch response{
            case .success(let data):
                if let errors = data["errors"].array{
                    errors.forEach { (error) in
                        self.alertMessage(message: error.stringValue)
                    }
                    
                }
                if let success = data["success"].bool, success{
                    self.alertMessage(message: "Готово")
                }
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
            getData()
            tableView.reloadData()
        }
    }
    
    func updateTask(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_TASK_UPDATE, method: .post, parameters: [
            "token" : UserDefaults.userToken,
            "id": model.id,
            "status_id": statuses[selectedStatus].id
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                if let errors = data["errors"].array{
                    errors.forEach { (error) in
                        self.alertMessage(message: error.stringValue)
                    }
                    
                }
                if let success = data["success"].bool, success{
                    self.alertMessage(message: "Готово")
                }
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        comments.removeAll()
        
        if let taskArray = data["children"].array{
            tasks = TaskModel.parseArray(data: taskArray)
        }
        
        if let commentsArray = data["comments"].array{
            for element in commentsArray{
                var comment = "no comment"
                var defuser = "unknown"
                
                if let param = element["param1"].string{
                    comment = param
                }
                
                if let user = element["user"]["fio"].string{
                    defuser = user
                }
                
                comments.append((user: defuser, comment: comment))
            }

        }
        files.removeAll()
        if let soundArray = data["sounds"].array {
            for element in soundArray {
                
                var user = "unknown"
                var sound = "unknow sound"
                
                if let sound2 = element["param1"].string{
                    sound = sound2
                }
                
                if let user2 = element["user"]["fio"].string{
                    user = user2
                }
                
                files.append((image: nil, user: user, voice: sound))
            }
        }
        
        if let imageArray = data["images"].array {
            for element in imageArray {
                
                var user = "unknown"
                var image = "unknown image"
                
                if let image2 = element["param1"].string{
                    image = image2
                }
                
                if let user2 = element["user"]["fio"].string{
                    user = user2
                }
                
                files.append((image: image, user: user, voice: nil))
            }
        }
        
        
        tableView.reloadData()
    }
    
    
    
}


extension TaskDetailVC : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
            if !statuses.isEmpty {
                return true
            }else{
                getStatus()
                return false
            }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func getStatus(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_TASK_STATUS, method: .get, parameters: ["token" : UserDefaults.userToken])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseStatuses(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    func parseStatuses(_ data : JSON){
        guard let elements = data.array else{
            return
        }
    
        for element in elements{
            statuses.append((id: element["id"].stringValue,
            value: element["name"].string ?? "error"))
        }
        DispatchQueue.main.async {
            self.statusTextField.becomeFirstResponder()
        }
        
    }
    
    
}
