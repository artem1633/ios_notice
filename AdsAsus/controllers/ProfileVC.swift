//
//  ProfileVC.swift
//  AdsAsus
//
//  Created by Abdullox on 8/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ProfileVC: UIViewController {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specialistLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var noAvatarImageView: UIImageView!
    
    let imagePickerController = UIImagePickerController()
    
    @IBOutlet weak var asd: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            imagePickerController.sourceType = .photoLibrary
            if let availableMediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary){
                print(availableMediaTypes)
                imagePickerController.mediaTypes = availableMediaTypes
            }
            
        }
        
        // Do any additional setup after loading the view.
        
        getData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func closeButtonClicked(_ sender: Any) {
        
        let st = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = st.instantiateViewController(identifier: "loginVC")
        
        UserDefaults.clear()
        present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func loadAvatar(_ sender: Any) {
        pickImage()
    }
    
    
    func getData(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_USER_DATA, method: .get, parameters: ["token" : UserDefaults.userToken])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseData(data: data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
    }
    
    func parseData(data : JSON){
        if let name = data["fio"].string{
            UserDefaults.userName = name
            nameLabel.text = name
        }
        
        if let permission = data["permission"].string{
            UserDefaults.userPermission = permission
            specialistLabel.text = permission
        }
        
        if let login = data["login"].string{
            UserDefaults.userLogin = login
            loginLabel.text = login
        }
        
        if let email = data["email"].string{
            UserDefaults.userEmail = email
            emailLabel.text = email
        }
        
        if let fcm_token = data["fcm_token"].string{
            UserDefaults.userFcmToken = fcm_token
        }
        
        if let avatar = data["avatar"].string{
            let imageUrl = "https://notice.teo-crm.com/\(avatar)"
            UserDefaults.avatar = imageUrl
            avatarImageView.load(url: imageUrl)
        }else{
            noAvatarImageView.isHidden = false
        }
    }
}


extension ProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            return self.pickerController(picker, didSelect: nil)
        }
        self.pickerController(picker, didSelect: image)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        pickerController(picker, didSelect: nil)
    }
    
    
    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)

        if let image = image{
            
            uploadImage(image: image)
            
        }
        
    }
    
    
    func pickImage(){
        let alert = UIAlertController(title: "Upload Image", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "From Camera", style: .default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                
                self.imagePickerController.sourceType = .camera
                if let availableMediaTypes = UIImagePickerController.availableMediaTypes(for: .camera){
                    print("availableMediaTypes ", availableMediaTypes)
                    self.imagePickerController.mediaTypes = availableMediaTypes
                    
                    self.imagePickerController.modalPresentationStyle = .fullScreen
                    self.present(self.imagePickerController, animated: true, completion: nil)
                }
                
            }
            
        }
        
        let libraryAction = UIAlertAction(title: "From Library", style: .default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                
                self.imagePickerController.sourceType = .photoLibrary
                if let availableMediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary){
                    print("availableMediaTypes ", availableMediaTypes)
                    self.imagePickerController.mediaTypes = availableMediaTypes
                    
                    self.imagePickerController.modalPresentationStyle = .popover
                    self.present(self.imagePickerController, animated: true, completion: nil)
                }
                
            }
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(cameraAction)
        alert.addAction(libraryAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    
    func uploadImage(image: UIImage) {
        SVProgressHUD.show()
        
        
        let imgData = image.jpegData(compressionQuality: 0.2)!
        
        
        Alamofire.upload(multipartFormData: {
            (multiFormData) in
                multiFormData.append(imgData, withName: "file",fileName: "avatar.jpg", mimeType: "image/jpg")
            multiFormData.append(UserDefaults.userToken.data(using: String.Encoding.utf8)!, withName: "token")
                            
            },
                         to: Constants.URL_LOAD_AVATAR, method: .post)
            
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        SVProgressHUD.dismiss()
                        print("response: \(response.result)")
                        print("response: \(response.response)")
                                           
                               
                        if response.result.isSuccess {
                                if response.response?.statusCode != 200 {
                                self.alertMessage(message: "Something is wrong, check network settings")
                            }else{
                                    UserDefaults.avatar = "avatar"
                                    self.avatarImageView.image = image
                                    self.noAvatarImageView.isHidden = true
                            }
                        }else{
                            self.alertMessage(message: "Something is wrong, check network settings")
                        }
                        
                        
                       
                    }

                case .failure(let encodingError):
                    print("failure: \(encodingError)")
                }
        }
        
    }
    
}
