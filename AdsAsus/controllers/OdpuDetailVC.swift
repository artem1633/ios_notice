//
//  OdpuDetailVC.swift
//  AdsAsus
//
//  Created by Abdullox on 8/26/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class OdpuDetailVC: UIViewController{
    
    enum Status {
        case info
        case history
    }
    
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var addressTextLabel: UILabel!
    @IBOutlet weak var dateTextLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var odpuId : String!
    var model: OdpuModel!
    var status : Status = .info

    var history : [(value: String, date: String)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = model.note + model.number
        addressTextLabel.text = model.address
        dateTextLabel.text = model.date_verify

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.backgroundColor = #colorLiteral(red: 0.9499999881, green: 0.9499999881, blue: 0.9499999881, alpha: 1)
        tableView.allowsSelection = false
        
        
        // Do any additional setup after loading the view.
        getData()
    }
    
    
    
    @IBAction func segmentItemChanged(_ sender: Any) {
        if segmentControl.selectedSegmentIndex == 0{
            infoView.isHidden = false
            tableView.isHidden = true
        }else{
            infoView.isHidden = true
            tableView.isHidden = false
        }
        
        switch segmentControl.selectedSegmentIndex {
        case 0:
            status = .info
        case 1:
            status = .history
        default:
            break
        }
        
        tableView.reloadData()
    }
    
}



extension OdpuDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch status {
        case .info:
            return 0
        case .history:
            return history.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "odpuHistoryTableCell", for: indexPath) as? OdpuHistoryTableCell{
            cell.model = history[indexPath.row]
            return cell
        }
        
        
        
        return UITableViewCell()
        
    }
    
}


extension OdpuDetailVC{
    
    func getData(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_ODPUS_DETAIL, method: .get, parameters: [
            "token" : UserDefaults.userToken,
            "meter_id": model.id
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        
        guard let list =  data.array else{
            return
        }
        
        
        for element in list{
            var value = "unknown"
            
            if let val = element["value"].double {
                value = "\(val)"
            }
            
            history.append((value: value,
                            date: element["created_at"].stringValue))
            
        }
        
        print("data: \(history.count)")
        tableView.reloadData()
        
    }
    
}
