//
//  HomeDetailVC.swift
//  AdsAsus
//
//  Created by Abdullox on 8/26/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class HomeDetailVC: UIViewController{
    
    enum HomeDetailStatus {
        case info
        case tasks
        case flames
        case odpus
    }
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoTextLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    var homeId : String!
    var text : String!
    var status : HomeDetailStatus = .info

    var tasks : [TaskModel] = []
    var flames : [FlameModel] = []
    var odpus : [OdpuModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = text
        infoTextLabel.text = text

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.backgroundColor = #colorLiteral(red: 0.9499999881, green: 0.9499999881, blue: 0.9499999881, alpha: 1)
        tableView.separatorStyle = .none
        
        tableView.register(UINib(nibName: "TaskTableCellTableViewCell", bundle: nil), forCellReuseIdentifier: "taskTableCellTableViewCell")
        tableView.register(UINib(nibName: "CrashFlameTableCell", bundle: nil), forCellReuseIdentifier: "crashFlameTableCell")
        tableView.register(UINib(nibName: "OdpuTableCell", bundle: nil), forCellReuseIdentifier: "odpuTableCell")
        // Do any additional setup after loading the view.
        getData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func segmentItemChanged(_ sender: Any) {
        if segmentControl.selectedSegmentIndex == 0{
            infoView.isHidden = false
            tableView.isHidden = true
        }else{
            infoView.isHidden = true
            tableView.isHidden = false
        }
        
        switch segmentControl.selectedSegmentIndex {
        case 0:
            status = .info
            addButton.isEnabled = false
        case 1:
            status = .tasks
            addButton.isEnabled = true
        case 2:
            status = .flames
            addButton.isEnabled = false
        case 3:
            status = .odpus
            addButton.isEnabled = false
        default:
            break
        }
        
        tableView.reloadData()
    }
    
}



extension HomeDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch status {
        case .info:
            return 0
        case .tasks:
            return tasks.count
        case .flames:
            return flames.count
        case .odpus:
            return odpus.count
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch status {
        case .tasks:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "taskTableCellTableViewCell", for: indexPath) as? TaskTableCellTableViewCell{
                cell.model = tasks[indexPath.row]
                return cell
            }
        case .flames:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "crashFlameTableCell", for: indexPath) as? CrashFlameTableCell{
                cell.model = flames[indexPath.row]
                return cell
            }
            case .odpus:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "odpuTableCell", for: indexPath) as? OdpuTableCell{
                cell.model = odpus[indexPath.row]
                return cell
            }
        default:
            return UITableViewCell()
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch status {
        case .tasks:
            tableView.deselectRow(at: indexPath, animated: true)
            let st = UIStoryboard(name: "Main", bundle: nil)
            let vc = st.instantiateViewController(identifier: "taskDetailVC") as! TaskDetailVC
            vc.model = tasks[indexPath.row]
            
            navigationController?.pushViewController(vc, animated: true)
            break
        case .flames:
            tableView.deselectRow(at: indexPath, animated: true)
            let st = UIStoryboard(name: "Main", bundle: nil)
            let vc = st.instantiateViewController(identifier: "flameDetailVC") as! FlameDetailVC
            vc.model = flames[indexPath.row]
            
            navigationController?.pushViewController(vc, animated: true)
            break
        case .odpus:
            tableView.deselectRow(at: indexPath, animated: true)
            let st = UIStoryboard(name: "Main", bundle: nil)
            let vc = st.instantiateViewController(identifier: "odpuDetailVC") as! OdpuDetailVC
            vc.model = odpus[indexPath.row]
            
            
            navigationController?.pushViewController(vc, animated: true)
            break
        default:
            break
        }
    }
    
}


extension HomeDetailVC{
    
    func getData(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_HOME_DETAIL, method: .get, parameters: [
            "token" : UserDefaults.userToken,
            "id": homeId!
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        
        
        if let taskArray = data["tasks"].array{
            tasks = TaskModel.parseArray(data: taskArray)
        }
        
        if let flameArray = data["crashDisables"].array{
            flames = FlameModel.parseArray(data: flameArray)
        }
        
        
        if let metersArray = data["meters"].array{
            odpus = OdpuModel.parseArray(data: metersArray)
        }
        
        print("tasks: \(tasks.count)")
        print("tasks: \(flames.count)")
        print("tasks: \(odpus.count)")
        
        tableView.reloadData()
        
    }
    
}
