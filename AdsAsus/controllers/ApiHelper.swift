//
//  ApiHelper.swift
//  AdsAsus
//
//  Created by Abdullox on 8/23/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import Alamofire

class ApiHelper{
    
    static func login(login: String, password: String) -> [String: Any]{
        return
            [
                "username" : login,
                "password": password
        ]
        
    }
    
    
    
    static func getItemDetail(id: Int) -> [String: Any]{
        return
            [
                "id" : id,
                 
        ]
        
    }
    
    
    static func getPremiumItems(offset: Int = 1, limit: String = "4") -> [String: String]{
        return
            [
                "offset" : "\(offset)",
                "limit": limit,
                 
                "status": "1"
        ]
        
    }
    
    
    

    static func getFixedItems(offset: Int = 1, limit: String = "4") -> [String: String]{
        return
            [
                "offset" : "\(offset)",
                "limit": limit,
                 
                "status": "2"
        ]
        
    }
    
    static func getItemsWithCategory(offset: Int, catId: Int) -> [String: Any]{
        return
            [
                "page" : offset,
                 
                "cat_id": catId
        ]
        
    }
    
    static func getStoreList(offset: Int, type: String) -> [String: String]{
        return
            [
                "page" : "\(offset)",
                 
                "type": type
        ]
        
    }
    
    static func getStoreItemsList(_ categoryId: Int,offset: Int) -> [String: Any]{
        return
            [
                "page" : "\(offset)",
                "id": categoryId,
                 
        ]
        
    }
    
    
    
    
    
    static func getStoreDetail(id: Int) -> [String: Any]{
        return
            [
                "id" : id,
                 
        ]
        
    }
    
    
    //Universal for login-password and telephone-password
    static func signIn(login: String, password: String) -> [String: Any]{
        return
            [
                "login": login,
                "password": password
        ]
    }
    
    static func signUp(phone: String, password: String) -> [String: Any]{
        return
            [
            "login": phone,
            "password": password
        ]
//                "lang_code": LanguageHelper.apiLang(),
        
    }
    
    static func verifySign(number: String,code: String) -> [String: Any]{
        return
            [
                "login": number,
                "code": code
        ]
    }
    
    static func resetUser(login: String) -> [String: String]{
        return
            [
                "login": login
//                "lang_code": LanguageHelper.apiLang()
        ]
    }
    
    static func resetUserContinue(login: String, code: String, password: String) -> [String: String]{
        return
            [
                "login": login,
                "code": code,
                "password": password
        ]
    }
    
    static func updateUser(token: String, option : [String: String]) -> [String: String]{
        return
            [
                "user_token": token
                ].merging(option, uniquingKeysWith: { (a, b) -> String in
                    a
                })
    }
    
//    1 => Ошибка на сайте
//    2 => Технический вопрос
//    3 => Предложение
//    4 => Другие вопросы
    static func contact(name: String, email: String, theme: Int, message: String) -> [String: Any]{
        return
            [
                 
                "name": name,
                "email": email,
                "message": message,
                "type": theme,
        ]
    }
//
//    majburiy ustun
//    1 => xamma chatdagi foydalanuvchilarni ko’rish uchun
//    2 => fatat magazinga kelgan xabarlar uchun
//    3 => oddiy chat uchun
        static func getUserList(type: String) -> [String: Any]{
            return
                [
                     
                    "type": type,
            ]
        }
    
//    bu majburiy ustun
//    1 => oddiy chat uchun
//    2 => kommentariya uchun
    static func getChatWithUser(id: String, type: String) -> [String: Any]{
                return
                    [
                         
                        "chat_id": id,
                        "active": type,
                ]
            }
    
    
    static func sendMessageToUser(id: Int, message : String) -> [String: Any]{
        return
            [
                
                "id": id,
                "type": "2",
                "message": message
        ]
    }
    
    
    //MARK: - PROFILE
    
    static func editUserProfile(fio: String, gender : String) -> [String: Any]{
           return
               [
                   "fio": fio,
                   "gender": gender
           ]
    }
    
    static func editUserLogin(login: String) -> [String: Any]{
        return
            [
                "login": login
        ]
    }
       
    
    
    //MARK: - ANNOUNCEMENT SECTION
    
 
    
    
       //MARK: - STORE SECTION
       
       static func createNewStore() -> [String: Any]{
           return
            [:
           ]
       }
    
    
}


