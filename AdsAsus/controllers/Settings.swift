//
//  Settings.swift
//  BisyorTrade
//
//  Created by Azimjon Nu'monov on 7/15/19.
//  Copyright © 2019 Nodir Group. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults{
    
    
    static var userToken : String{
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.KEY_USER_TOKEN)
        }
        get{
            return standard.string(forKey: Constants.KEY_USER_TOKEN) ?? ""
        }
    }
    
    static var userFcmToken : String{
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.KEY_USER_FCM_TOKEN)
        }
        get{
            return standard.string(forKey: Constants.KEY_USER_FCM_TOKEN) ?? ""
        }
    }
    
    
    
    static var userPin : String{
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.KEY_USER_PIN)
        }
        get{
            return UserDefaults.standard.string(forKey: Constants.KEY_USER_PIN) ?? ""
        }
    }
    
    
    static var userName : String?{
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.KEY_USER_NAME)
            userLogin = newValue!
        }
        get{
            return UserDefaults.standard.string(forKey: Constants.KEY_USER_NAME)
        }
    }
    
    static var userPermission : String?{
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.KEY_USER_PERMISSION)
            userLogin = newValue!
        }
        get{
            return UserDefaults.standard.string(forKey: Constants.KEY_USER_PERMISSION)
        }
    }

    static var email : String?{
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.KEY_USER_EMAIL)
            if userLogin.isEmpty{
                userLogin = newValue!
            }
        }
        get{
            return UserDefaults.standard.string(forKey: Constants.KEY_USER_EMAIL)
        }
    }
    
    
    static var avatar : String?{
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.KEY_USER_AVATAR)
        }
        get{
            return UserDefaults.standard.string(forKey: Constants.KEY_USER_AVATAR)
        }
    }
    
    
    static var userLogin : String{
          set{
              UserDefaults.standard.set(newValue, forKey: Constants.KEY_USER_LOGIN)
          }
          get{
              return UserDefaults.standard.string(forKey: Constants.KEY_USER_LOGIN) ?? ""
          }
      }
    
    static var userEmail : String{
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.KEY_USER_EMAIL)
        }
        get{
            return UserDefaults.standard.string(forKey: Constants.KEY_USER_EMAIL) ?? ""
        }
    }
    
    
    static func clear(){
        standard.removeObject(forKey: Constants.KEY_USER_TOKEN)
        standard.removeObject(forKey: Constants.KEY_USER_FCM_TOKEN)
        standard.removeObject(forKey: Constants.KEY_USER_PIN)
        standard.removeObject(forKey: Constants.KEY_USER_NAME)
        standard.removeObject(forKey: Constants.KEY_USER_PERMISSION)
        standard.removeObject(forKey: Constants.KEY_USER_EMAIL)
        standard.removeObject(forKey: Constants.KEY_USER_AVATAR)
        standard.removeObject(forKey: Constants.KEY_USER_LOGIN)
        standard.removeObject(forKey: Constants.KEY_USER_EMAIL)
        
        
    }
    
}
