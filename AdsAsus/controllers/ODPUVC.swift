//
//  ODPUVC.swift
//  AdsAsus
//
//  Created by Abdullox on 8/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class ODPUVC: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    
    var odpus : [OdpuModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.backgroundColor = #colorLiteral(red: 0.9499999881, green: 0.9499999881, blue: 0.9499999881, alpha: 1)
        tableView.separatorStyle = .none
        
        tableView.register(UINib(nibName: "OdpuTableCell", bundle: nil), forCellReuseIdentifier: "odpuTableCell")
        // Do any additional setup after loading the view.
        getData()
        if odpus.count == 0 {
            presentAlert()
        }
    }
    
    func presentAlert() {
        let ac = UIAlertController(title: nil, message: "B данном разделе информация отображается только после внесения показаний диспетчером", preferredStyle: .alert)
        let close = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
        ac.addAction(close)
        present(ac, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
}



extension ODPUVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return odpus.count
       
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier:"odpuTableCell", for: indexPath) as? OdpuTableCell{
            cell.model = odpus[indexPath.row]
            return cell
        }
    
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let st = UIStoryboard(name: "Main", bundle: nil)
        let vc = st.instantiateViewController(identifier: "odpuDetailVC") as! OdpuDetailVC
        vc.model = odpus[indexPath.row]
        
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension ODPUVC{
    
    func getData(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_ODPUS_ALL, method: .get, parameters: [
            "token" : UserDefaults.userToken,
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        
        if let metersArray = data.array{
            odpus = OdpuModel.parseArray(data: metersArray)
        }
        
        print("odpus: \(odpus.count)")
        tableView.reloadData()
        
    }
    
}

