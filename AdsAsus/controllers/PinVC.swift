//
//  PinVC.swift
//  AdsAsus
//
//  Created by Abdullox on 8/21/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import KAPinField

class PinVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var pinField: KAPinField!
    
    var message : String!
    
    var changePin = false
    var firstStep = true
    var savedCode = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UserDefaults.userPin.isEmpty {
            changePin = true
        }
        
        pinField.properties.delegate = self

        // Do any additional setup after loading the view.
        
        pinField.properties.animateFocus = true // Animate the currently focused token
        pinField.properties.isSecure = true // Secure pinField will hide actual input
        pinField.properties.secureToken = "*"
        
        pinField.becomeFirstResponder()
        
        if changePin {
            titleLabel.text = "Придумайте новый пароль"
        }
        
        
        
        
        
//        pinField.appearance.kerning = 20 // Space between characters, default to 16
//        pinField.appearance.textColor = UIColor.white.withAlphaComponent(1.0) // Default to nib color or black if initialized programmatically.
//        pinField.appearance.tokenColor = UIColor.black.withAlphaComponent(0.3) // token color, default to text color
//        pinField.appearance.tokenFocusColor = UIColor.black.withAlphaComponent(0.3)  // token focus color, default to token color
//        pinField.appearance.backOffset = 8 // Backviews spacing between each other
//        pinField.appearance.backColor = UIColor.clear
//        pinField.appearance.backBorderWidth = 1
//        pinField.appearance.backBorderColor = UIColor.white.withAlphaComponent(0.2)
//        pinField.appearance.backCornerRadius = 4
//        pinField.appearance.backFocusColor = UIColor.clear
//        pinField.appearance.backBorderFocusColor = UIColor.white.withAlphaComponent(0.8)
//        pinField.appearance.backActiveColor = UIColor.clear
//        pinField.appearance.backBorderActiveColor = UIColor.white
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        view.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension PinVC : KAPinFieldDelegate {
  func pinField(_ field: KAPinField, didFinishWith code: String) {
    print("didFinishWith : \(code)")
    
    
    guard changePin else {
        if UserDefaults.userPin == code {
            performSegue(withIdentifier: "toMainVC", sender: self)
        }else{
            pinField.animateFailure()
            pinField.text?.removeAll()
        }
        
        return
    }
    
        
    if firstStep {
        titleLabel.text = "Подтвердите новый пароль"
        pinField.text = ""
        pinField.reloadAppearance()
        firstStep = false
        savedCode = code
    }else if savedCode == code{
        UserDefaults.userPin = code
        performSegue(withIdentifier: "toMainVC", sender: self)
    }else{
        pinField.text?.removeAll()
        pinField.animateFailure()
        
        titleLabel.text = "Придумайте новый пароль"
        firstStep = true
    }
    
  }
    
}
