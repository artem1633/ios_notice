//
//  Enums.swift
//  BisyorTrade
//
//  Created by Abdullox on 7/19/20.
//  Copyright © 2020 Nodir Group. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

enum ServerResponse{
    case success(JSON)
    case failure(String)
}


extension UIViewController{
    
    func makeRequest(request req: DataRequest,compilation: @escaping ((ServerResponse) -> Void)) {
        
        let url : String = req.request?.url?.absoluteString ?? "undefined url: "
        
        print("\(req)")
        
        req.responseJSON { (response) in
            self.view.isUserInteractionEnabled = true
            SVProgressHUD.dismiss()
            
            if response.result.isSuccess {
                let data : JSON = JSON(response.result.value!)
                print("\(url): \(data)")
                    
                if response.response?.statusCode != 200 {
                    compilation(.failure("Something is wrong, check network settings"))
                }else{
                    compilation(.success(data))
                }
            }else{
                SVProgressHUD.dismiss()
                
                print("Failure(\(url)): " + String(describing: response.result.error))
                compilation(.failure("Something is wrong, check network settings"))
            }
        }

        
    }
    
    
    func makeRequestWithImage(url: String, image: UIImage,withName: String, with params: [String: String],compilation: @escaping ((ServerResponse) -> Void)){
        
        let imgData = image.jpegData(compressionQuality: 0.2)!
        
        
        Alamofire.upload(multipartFormData: {
            (multiFormData) in
                multiFormData.append(imgData, withName: withName,fileName: "avatar.jpg", mimeType: "image/jpg")
            
                for (key, value) in params {
                    
                    multiFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                } //Optional for extra parameters
            },
            to: url, method: .post,
            headers: ["Authorization" : UserDefaults.userToken])
            
            { (result) in
                switch result {
                case .success(let upload, _, _):

                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })

                    upload.responseJSON { response in
                        self.view.isUserInteractionEnabled = true
                        SVProgressHUD.dismiss()
                                           
                               
                        if response.result.isSuccess {
                            let data : JSON = JSON(response.result.value!)
                            print("\(url): \(data)")
                                
                            if response.response?.statusCode != 200 {
                                compilation(.failure(data["message"].string ?? "something is wrong"))
                            }else{
                                compilation(.success(data))
                            }
                        }else{
                            SVProgressHUD.dismiss()
                            
                            print("Failure(\(url)): " + String(describing: response.result.error))
                            compilation(.failure("Something is wrong, check network settings"))
                        }
                        
                       
                    }

                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        
    }
    
    
}

