//
//  NewTaskVC.swift
//  AdsAsus
//
//  Created by Abdullox on 8/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class NewTaskVC: UIViewController{

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var petetionTextField: PaddingTextField!
    @IBOutlet weak var statusTextField: PaddingTextField!
    @IBOutlet weak var homeTextField: PaddingTextField!
    @IBOutlet weak var usersTextField: PaddingTextField!
    @IBOutlet weak var isPaidSwitcher: UISwitch!
    @IBOutlet weak var prioritetSwitcher: UISwitch!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var simpleDescriptionTextView: UITextView!
    @IBOutlet weak var fullDescriptionTextView: UITextView!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    
    var selectedPetetion : Int = -1
    var selectedStatus : Int = -1
    var selectedHome : Int = -1
    var selectedUser : Int = -1
    
    var petetions : [(id: String, value: String)] = []
    var statuses : [(id: String, value: String)] = []
    var homes : [(id: String, value: String)] = []
    var users : [(id: String, value: String)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        petetionTextField.delegate = self
        statusTextField.delegate = self
        homeTextField.delegate = self
        usersTextField.delegate = self
        simpleDescriptionTextView.delegate = self
        fullDescriptionTextView.delegate = self
        configurePickerView()
        
        // Do any additional setup after loading the view.
    
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleKeyboardNotification(notification: NSNotification){
        
        guard simpleDescriptionTextView.isFirstResponder || fullDescriptionTextView.isFirstResponder else{
            return
        }
        
        if let userInfo = notification.userInfo{
            let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            let height: CGFloat = keyboardSize?.height ?? 0
            
            
            self.viewHeightConstraint.constant = isKeyboardShowing ? height : 1

            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }) { (completion) in
                
                if isKeyboardShowing {
                    let contentOffset = CGPoint(x: 0, y: self.scrollView.contentSize.height - self.scrollView.bounds.size.height)
                    self.scrollView.setContentOffset(contentOffset, animated: true)
                }
            }
            
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func continueButtonClicked(_ sender: Any) {
        guard selectedPetetion != -1 else{
            alertMessage(message: "Заполните поля")
            
            return
        }
        
        guard selectedStatus != -1 else{
            alertMessage(message: "Заполните поля")
            return
        }
        
        guard selectedHome != -1 else{
            alertMessage(message: "Заполните поля")
            return
        }
        
        guard selectedUser != -1 else{
            alertMessage(message: "Заполните поля")
            return
        }
        
        continueCreating()
    }
    
    func configurePickerView(){
        
        let pickerView1 = UIPickerView()
        let pickerView2 = UIPickerView()
        let pickerView3 = UIPickerView()
        let pickerView4 = UIPickerView()
        pickerView1.delegate = self
        pickerView1.dataSource = self
        pickerView2.delegate = self
        pickerView2.dataSource = self
        pickerView3.delegate = self
        pickerView3.dataSource = self
        pickerView4.delegate = self
        pickerView4.dataSource = self
        
        pickerView1.tag = 100
        pickerView2.tag = 101
        pickerView3.tag = 102
        pickerView4.tag = 103
        
        let toolbar = UIToolbar()
                
        toolbar.sizeToFit()
                
        let doneButton = UIBarButtonItem(title: "Выбрать", style: .plain, target: self, action: #selector(pickerViewDoneClicked))
                
        toolbar.setItems([doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        
        petetionTextField.inputView = pickerView1
        petetionTextField.inputAccessoryView = toolbar
        
        statusTextField.inputView = pickerView2
        statusTextField.inputAccessoryView = toolbar
        
        homeTextField.inputView = pickerView3
        homeTextField.inputAccessoryView = toolbar
        
        usersTextField.inputView = pickerView4
        usersTextField.inputAccessoryView = toolbar
    }
    
    @objc func pickerViewDoneClicked(sender: Any){
        view.endEditing(true)
    }
    
}


extension NewTaskVC : UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
        case 100:
            return petetions.count
        case 101:
            return statuses.count
        case 102:
            return homes.count
        case 103:
            return users.count
        default:
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 100:
            return petetions[row].value
        case 101:
            return statuses[row].value
        case 102:
            return homes[row].value
        case 103:
            return users[row].value
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 100:
            selectedPetetion = row
            petetionTextField.text = petetions[row].value
            break
        case 101:
            selectedStatus = row
            statusTextField.text = statuses[row].value
            break
        case 102:
            selectedHome = row
            homeTextField.text = homes[row].value
            break
        case 103:
            selectedUser = row
            usersTextField.text = users[row].value
            break
        default:
            break
        }
    }
    
    
}


extension NewTaskVC : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case  petetionTextField:
            if !petetions.isEmpty {
                return true
            }else{
                getPetetion()
                return false
            }

            
        case statusTextField:
            if !statuses.isEmpty {
                return true
            }else{
                getStatus()
                return false
            }
        case homeTextField:
            if !homes.isEmpty {
                return true
            }else{
                getHomes()
                return false
            }
        case usersTextField:
            if !users.isEmpty {
                return true
            }else{
                getUsers()
                return false
            }
        default:
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
}


extension NewTaskVC : UITextViewDelegate{
    
      func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        view.endEditing(true)
        
        return true
      }
    
}



extension NewTaskVC {
    //MARK: - NETWORKING
    
    
    func continueCreating(){
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        
        var timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "YYYY-MM-DD"
        
        var params : [String: Any] = [:]
        params["token"] = UserDefaults.userToken
        params["petition_id"] = petetions[selectedPetetion].id
        params["house_id"] = homes[selectedHome].id
        params["executor_id"] = users[selectedUser].id
        params["status_id"] = statuses[selectedStatus].id
        params["is_fee"] = isPaidSwitcher.isOn ? "1" : "0"
        params["priority"] = prioritetSwitcher.isOn ? "1" : "0"
        params["execute_datetime"] = timeFormatter.string(from: datePicker.date) + " 00:00:00"
        params["description"] = fullDescriptionTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        params["description_short"] = simpleDescriptionTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        let req = Alamofire.request(Constants.URL_TASK_CREATE, method: .post, parameters: params, encoding: URLEncoding.default)
        
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                if let errors = data["errors"].array{
                    errors.forEach { (error) in
                        self.alertMessage(message: error.stringValue)
                    }
                    
                }
                if let success = data["success"].bool, success{
                    self.navigationController?.popViewController(animated: true)
                }
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    func getPetetion(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_TASK_PETETIONS, method: .post, parameters: ["token" : UserDefaults.userToken], encoding: URLEncoding.default)
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parsePetetions(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    func getStatus(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_TASK_STATUS, method: .get, parameters: ["token" : UserDefaults.userToken])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseStatuses(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    func getHomes(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_TASK_HOME, method: .get, parameters: ["token" : UserDefaults.userToken])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseHomes(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    func getUsers(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_TASK_USERS, method: .get, parameters: ["token" : UserDefaults.userToken])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseUsers(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    //MARK: - PARSING
    
    func parsePetetions(_ data : JSON){
        guard let elements = data.array else{
            return
        }
        
        for element in elements{
            petetions.append((id: element["id"].stringValue,
                    value: element["header"].string ?? "error"))
        }
        DispatchQueue.main.async {
            self.petetionTextField.becomeFirstResponder()
        }
        
        
    }
    
    func parseStatuses(_ data : JSON){
        guard let elements = data.array else{
            return
        }
    
        for element in elements{
            statuses.append((id: element["id"].stringValue,
            value: element["name"].string ?? "error"))
        }
        DispatchQueue.main.async {
            self.statusTextField.becomeFirstResponder()
        }
        
    }
    
    func parseHomes(_ data : JSON){
        guard let elements = data.array else{
            return
        }
        
        for element in elements{
            homes.append((id: element["id"].stringValue,
            value: element["address"].string ?? "error"))
        }
        DispatchQueue.main.async {
            self.homeTextField.becomeFirstResponder()
        }
        
    }
    
    func parseUsers(_ data : JSON){
        guard let elements = data.array else{
            return
        }
        
        
        for element in elements{
            users.append((id: element["id"].stringValue,
            value: element["fio"].string ?? "error"))
        }
        DispatchQueue.main.async {
            self.usersTextField.becomeFirstResponder()
        }
        
    }
    
}
