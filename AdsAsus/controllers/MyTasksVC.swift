//
//  MyTasksVC.swift
//  AdsAsus
//
//  Created by Abdullox on 8/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class MyTasksVC: UIViewController{
    
    
    @IBOutlet weak var segmentScroll: UIScrollView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var status : StatusModel.Status = .new

    var tasks : [Int : [TaskModel]] = [:]
    
    let refreshControl : UIRefreshControl = {
            let refresh = UIRefreshControl()
            refresh.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            refresh.addTarget(self, action: #selector(refreshWindow), for: UIControl.Event.valueChanged)
        
        return refresh
    }()
    
    @objc func refreshWindow(sender: UITableView){
        refreshControl.endRefreshing()
        getData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshWindow), for: .allEvents)
    
        
        tableView.backgroundColor = #colorLiteral(red: 0.9499999881, green: 0.9499999881, blue: 0.9499999881, alpha: 1)
        tableView.separatorStyle = .none
        
        tableView.register(UINib(nibName: "TaskTableCellTableViewCell", bundle: nil), forCellReuseIdentifier: "taskTableCellTableViewCell")
        // Do any additional setup after loading the view.
        
       
       
        var scrollViewWidth:Float = 0.0
        let items  = ["Новое", "В работе", "Решено", "Отменено",  "Архивировано", "Просрочено"]
        for (index, element) in items.enumerated() {
            let size = element.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20.0)]) //you can change font as you want
            segmentControl.setWidth(size.width, forSegmentAt: index)
            scrollViewWidth = scrollViewWidth + Float(size.width)
        }
        segmentScroll.translatesAutoresizingMaskIntoConstraints = false
        segmentScroll.widthAnchor.constraint(equalToConstant: CGFloat(scrollViewWidth)).isActive = true
        

        // Here you can set content width of UIScollView by use of "scrollViewWidth"
        // scrollView.contentSize = CGSize(width: scrollViewWidth, height: 40)// height should whatever you want.
        
//        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        getData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func segmentItemChanged(_ sender: Any) {
        
       status = StatusModel.Status.init(rawValue: (segmentControl.selectedSegmentIndex + 1)) ?? .new
        
        tableView.reloadData()
    }
    
}



extension MyTasksVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list : [TaskModel] = tasks[status.rawValue]{
            return list.count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "taskTableCellTableViewCell", for: indexPath) as? TaskTableCellTableViewCell{
            cell.model = tasks[status.rawValue]![indexPath.row]
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let st = UIStoryboard(name: "Main", bundle: nil)
        let vc = st.instantiateViewController(identifier: "taskDetailVC") as! TaskDetailVC
        vc.model = tasks[status.rawValue]![indexPath.row]
        
        
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension MyTasksVC{
    
    func getData(){
        tasks.removeAll()
        tableView.reloadData()
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_TASKS_FREE, method: .get, parameters: [
            "token" : UserDefaults.userToken
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        
        tasks = TaskModel.parseStatus(data: data)
                
        tableView.reloadData()
        
    }
    
}
