//
//  Ехтенсионс.swift
//  EcoBox
//
//  Created by Abdullox on 8/31/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation

//
//  Extensions.swift
//  AdsAsus
//
//  Created by Abdullox on 8/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import Alamofire
import SVProgressHUD

extension UIImageView{
    func load(url: String) -> Void{
        
        self.sd_setImage(with:URL(string: url))
    }
    
    func changeColor(color: UIColor){
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
    
}


extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

