//
//  LoginVC.swift
//  AdsAsus
//
//  Created by Abdullox on 8/21/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import Firebase



class LoginVC: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginTextField.delegate = self
        passwordTextField.delegate = self

        // Do any additional setup after loading the view.
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(gestureRecogniser(recogniser:)))
        self.view.addGestureRecognizer(gesture)
    }
    
    @objc func gestureRecogniser(recogniser: UIPanGestureRecognizer){
        view.endEditing(true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func submit(_ sender: UIButton, forEvent event: UIEvent) {
        
        guard !(loginTextField.text?.isEmpty ?? true) else {
            alertMessage(message: "Логин поля пуста")
            return
        }
        
        guard !(loginTextField.text?.isEmpty ?? true) else {
            alertMessage(message: "Логин поля пуста")
            return
        }
        
        loginUser(login: loginTextField.text!, password: passwordTextField.text!)
    }
    
    func loginUser(login: String, password: String){
        
        let req = Alamofire.request(Constants.URL_LOGIN,
                                    method: .post,
                                    parameters: ApiHelper.login(login: login, password: password),
                                    encoding: URLEncoding.default)
    
        makeRequest(request: req) { (response) in
            switch (response){
            case let .success(data):
                
                if let result = data["result"].string {
                    print("\(result)")
                    UserDefaults.userToken = result
                    self.setFcmToken()
                }else if let message = data["errors"][0].string{
                    self.alertMessage(message: message)
                }
                
                break
            case let .failure(message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    func setFcmToken(){
        let req = Alamofire.request(Constants.URL_FCM_TOKEN,
                                    method: .post,
                                    parameters: ["token": UserDefaults.userToken, "fcm_token" : UserDefaults.userFcmToken],
                                    encoding: URLEncoding.default)
    
        makeRequest(request: req) { (response) in
            switch (response){
            case .success(_):
                self.performSegue(withIdentifier: "toPinVC", sender: self)
                break
            case .failure(_):
                self.performSegue(withIdentifier: "toPinVC", sender: self)
                break
            }
        }
        
    }
    
}


extension LoginVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
}



extension UIViewController{
    
    func alertMessage(message: String){
        let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(alertAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

