//
//  Constants.swift
//  BisyorTrade
//
//  Created by Azimjon Nu'monov on 7/15/19.
//  Copyright © 2019 Nodir Group. All rights reserved.
//

import Foundation

class Constants {
    public static let BASE_URL : String = "https://ads.asusmkd.ru/"
    
    
    public static let URL_LOGIN : String = Constants.BASE_URL + "api/user/get-token"
    public static let URL_FCM_TOKEN : String = Constants.BASE_URL + "api/user/set-fcm-token"
    public static let URL_USER_DATA : String = Constants.BASE_URL + "api/user/me"
    public static let URL_LOAD_AVATAR : String = Constants.BASE_URL + "api/user/upload-avatar"
    
    //MARK: - GET TASK PART
    public static let URL_TASKS_FREE : String = Constants.BASE_URL + "api/task/free"
    public static let URL_TASKS_PAID : String = Constants.BASE_URL + "api/task/not-free"
    public static let URL_TASKS_DETAIL : String = Constants.BASE_URL + "/api/task/properties"
    
    //MARK: - GET FLAME PART
    public static let URL_FLAME_ALL : String = Constants.BASE_URL + "api/crash-disable/index"
    public static let URL_FLAME_DETAIL : String = Constants.BASE_URL + "api/crash-disable/view"
    public static let URL_FLAME_UPDATE : String = Constants.BASE_URL + "api/crash-disable/update"
    
    
    //MARK: - GET TASK PART
    public static let URL_ODPUS_ALL : String = Constants.BASE_URL + "/api/meter/index"
    public static let URL_ODPUS_DETAIL : String = Constants.BASE_URL + "/api/meter-info/index"
    
    
    
    //MARK: - CREATE TASK PART
    public static let URL_TASK_PETETIONS : String = Constants.BASE_URL + "api/petition/index"
    public static let URL_TASK_STATUS : String = Constants.BASE_URL + "api/status/index"
    public static let URL_TASK_HOME : String = Constants.BASE_URL + "api/house/index"
    public static let URL_TASK_USERS : String = Constants.BASE_URL + "api/user/executors"
    public static let URL_TASK_CREATE : String = Constants.BASE_URL + "api/task/create"
    public static let URL_TASK_UPDATE : String = Constants.BASE_URL + "api/task/update"
    
    
    //MARK: - CREATE TASK PART
    public static let URL_HOME_ALL : String = Constants.BASE_URL + "api/house/index"
    public static let URL_HOME_DETAIL : String = Constants.BASE_URL + "api/house/properties"
    
    
    
    //MARK: - KEYS
    public static let KEY_FIRST_ENTER : String = "is_first_enter"
    public static let KEY_LOGINED : String = "is_logined"
    public static let KEY_USER_PIN : String = "key_user_pin"
    public static let KEY_USER_TOKEN : String = "key_user_token"
    public static let KEY_USER_LOGIN : String = "login"
    public static let KEY_USER_NAME : String = "name"
    public static let KEY_USER_PERMISSION : String = "permission"
    public static let KEY_USER_FCM_TOKEN : String = "fcm_token"
    public static let KEY_USER_EMAIL : String = "email"
    public static let KEY_USER_AVATAR : String = "avatar"
    
    //MARK: - COMMENT
    public static let URL_POST_COMMENT : String = Constants.BASE_URL + "/api/task/add-comment"
    public static let URL_POST_PHOTO : String = Constants.BASE_URL + "/api/task/add-image"
    public static let URL_POST_VOICE : String = Constants.BASE_URL + "/api/task/add-sound"
}
