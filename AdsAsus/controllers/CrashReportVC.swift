//
//  CrashReportVC.swift
//  AdsAsus
//
//  Created by Abdullox on 8/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class CrashReportVC: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    
    var flames : [FlameModel] = []
    
    let refreshControl : UIRefreshControl = {
            let refresh = UIRefreshControl()
            refresh.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            refresh.addTarget(self, action: #selector(refreshWindow), for: UIControl.Event.valueChanged)
        
        return refresh
    }()
    
    @objc func refreshWindow(sender: UITableView){
        refreshControl.endRefreshing()
        getData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
        
        tableView.backgroundColor = #colorLiteral(red: 0.9499999881, green: 0.9499999881, blue: 0.9499999881, alpha: 1)
        tableView.separatorStyle = .none
        
        
        tableView.register(UINib(nibName: "CrashFlameTableCell", bundle: nil), forCellReuseIdentifier: "crashFlameTableCell")
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
  
    
}



extension CrashReportVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return flames.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      
        if let cell = tableView.dequeueReusableCell(withIdentifier: "crashFlameTableCell", for: indexPath) as? CrashFlameTableCell{
                cell.model = flames[indexPath.row]
                return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let st = UIStoryboard(name: "Main", bundle: nil)
        let vc = st.instantiateViewController(identifier: "flameDetailVC") as! FlameDetailVC
        vc.model = flames[indexPath.row]
        
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}


extension CrashReportVC{
    
    func getData(){
        flames.removeAll()
        tableView.reloadData()
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_FLAME_ALL, method: .get, parameters: [
            "token" : UserDefaults.userToken
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        
        
        
        if let flameArray = data.array{
            flames = FlameModel.parseArray(data: flameArray)
        }
        
        tableView.reloadData()
        
    }
    
}

