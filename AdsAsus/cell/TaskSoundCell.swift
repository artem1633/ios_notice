//
//  TaskSoundCell.swift
//  AdsAsus
//
//  Created by Святослав Шевченко on 18.12.2020.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import AVFoundation

class TaskSoundCell: UITableViewCell, AVAudioPlayerDelegate {
    
    var audioPlayer:AVAudioPlayer!
    
    @IBOutlet weak var taskLabel: UILabel!
    var sound = ""
    
    @IBAction func playSound(_ sender: Any) {
        let url = URL(string: sound)
        downloadFileFromURL(url: url!)
    }
    
    func downloadFileFromURL(url: URL){
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url) { (url, response, error) in
            self.play(url: url!)
        }
        downloadTask.resume()
    }
    func play(url:URL) {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url as URL)
            audioPlayer.prepareToPlay()
            //audioPlayer.volume = 2.0
            audioPlayer.play()
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
