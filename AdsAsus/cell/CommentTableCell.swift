//
//  CommentTableCell.swift
//  AdsAsus
//
//  Created by Abdullox on 8/27/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class CommentTableCell: UITableViewCell {

    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    var model: (user: String, comment: String)!{
        didSet{
            userLabel.text = model.unsafelyUnwrapped.user
            commentLabel.text = model.unsafelyUnwrapped.comment
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
