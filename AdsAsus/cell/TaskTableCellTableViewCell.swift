//
//  TaskTableCellTableViewCell.swift
//  AdsAsus
//
//  Created by Abdullox on 8/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class TaskTableCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var starImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    
    var model : TaskModel!{
        didSet{
            addressLabel.text = model.address
            dateLabel.text = model.expiredDate
            descriptionLabel.text = model.description
            statusLabel.text = StatusModel.statuses[model.status]
            userLabel.text = model.user
            
            if model.hasPriority {
                starImageView.isHidden = false
            }
            numberLabel.text = "\(model.id)"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
