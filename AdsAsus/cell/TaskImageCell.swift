//
//  TaskImageCell.swift
//  AdsAsus
//
//  Created by Святослав Шевченко on 18.12.2020.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class TaskImageCell: UITableViewCell {
    
    @IBOutlet weak var taskImage: UIImageView!
    @IBOutlet weak var taskLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
