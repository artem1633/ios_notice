//
//  OdpuHistoryTableCell.swift
//  AdsAsus
//
//  Created by Abdullox on 8/26/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class OdpuHistoryTableCell: UITableViewCell {
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    var model: (value: String,date: String)!{
        didSet{
            valueLabel.text = model.unsafelyUnwrapped.value
            dateLabel.text = model.unsafelyUnwrapped.date
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
