//
//  CrashFlameTableCell.swift
//  AdsAsus
//
//  Created by Abdullox on 8/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class CrashFlameTableCell: UITableViewCell {

    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var factlabel: UILabel!
    @IBOutlet weak var resourceLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var homeLabel: UILabel!
    
    var model : FlameModel!{
        didSet{
            startDateLabel.text = model.startDate
            endDateLabel.text = model.endDate
            factlabel.text = model.endFact
            
            statusLabel.text = model.status
            homeLabel.text = model.address
            
            switch model.resource {
            case "0":
                resourceLabel.text = "Газ"
            case "1":
                resourceLabel.text = "Холодная вода"
            case "2":
                resourceLabel.text = "Электричество"
            case "3":
                resourceLabel.text = "Отопление"
            case "5":
                resourceLabel.text = "Горячая вода"
                
            default:
                break;
            }
            
            switch model.status {
            case "0":
                statusLabel.text = "Новый"
            case "1":
                statusLabel.text = "В работе"
            case "2":
                resourceLabel.text = "Ликвидировано"
            
            default:
                break;
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
